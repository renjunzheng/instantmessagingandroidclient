package com.zhengbros.rjz.instantmessaging;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import java.io.BufferedReader;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class listenToSocketService extends IntentService {
    public static final String BROADCAST_ACTION = "com.zhengbros.rjz.instantmessaging.displayinmessage";
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.zhengbros.rjz.instantmessaging.action.FOO";

    public listenToSocketService() {
        super("listenToSocketService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionFoo(Context context) {
        Intent intent = new Intent(context, listenToSocketService.class);
        intent.setAction(ACTION_FOO);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                handleActionFoo();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo() {
        String oneLineMessage;
        BufferedReader br = ChattingActivity.br;
        try {
            while ((oneLineMessage = br.readLine()) != null) {
                //broadcast message
                Intent intent = new Intent(BROADCAST_ACTION);
                intent.putExtra("newMessage", oneLineMessage);
                sendBroadcast(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
