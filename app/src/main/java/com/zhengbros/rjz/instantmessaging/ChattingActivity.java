package com.zhengbros.rjz.instantmessaging;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;


public class ChattingActivity extends AppCompatActivity {

    public static BufferedReader br = null;
    private final int serverPort = 5000;
    private final String serverIP = "128.46.76.113";
    private Socket socket = null;
    private PrintWriter pw = null;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUI(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        //make the TextView scrollable and add line after
        TextView tv = (TextView) findViewById(R.id.allMessages);
        tv.setMovementMethod(new ScrollingMovementMethod());
        tv.setSelected(true);

        new Thread(new initializeSocketThread()).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chatting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendButtonOnClick(View view) {
        EditText et = (EditText) findViewById(R.id.outMessage);
        String outMessage = et.getText().toString();
        outMessage = packMessage(outMessage);
        try {
            pw.print(outMessage);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            et.setText("");
        }
    }

    private String packMessage(String rawMessage) {
        return rawMessage;
    }

    private void updateUI(Intent intent) {
        String inMessage = intent.getStringExtra("newMessage");
        TextView tv = (TextView) findViewById(R.id.allMessages);
        String existMessages = tv.getText().toString();
        tv.setText(existMessages + inMessage + "\n");
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(listenToSocketService.BROADCAST_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    class initializeSocketThread implements Runnable {

        @Override
        public void run() {
            try {
                InetAddress serverAddress = InetAddress.getByName(serverIP);
                socket = new Socket(serverAddress, serverPort);

                pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                listenToSocketService.startActionFoo(getApplicationContext());
                Log.i("initializeSocket", "finished");
                //new Thread(new listenToSocketThread()).start();
            } catch (IOException e2) {
                //throwing by socket
                e2.printStackTrace();
            }
        }
    }
}
